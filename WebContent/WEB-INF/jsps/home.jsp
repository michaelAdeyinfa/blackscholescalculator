<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Black Scholes Quote Calculator</title>
<link rel="stylesheet" type="text/css" href="css/semantic.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
<script src="js/semantic.min.js"></script>
<script>
	$('.ui.checkbox').checkbox();
</script>
</head>
<body>
	<div style="text-align: center">
		<h1>Black-Scholes Quote Calculator</h1>
	</div>
	<br>
	<br>
	<c:url var="url" value="/calculateQuote" />
	<form method="POST" action="${url}">
		<div class="ui equal width form">
			<div class="fields">
				<div class="field">
					<label>Strike Price</label> <input type="text" name="strikePrice"
						placeholder="Strike Price">
				</div>
				<div class="field">
					<label>Stock Price</label> <input type="text" name="stockPrice"
						placeholder="Stock Price">
				</div>
			</div>
			<div class="fields">
				<div class="field">
					<label>Time To Expiration</label> <input type="text" name="time"
						placeholder="Number of Days">
				</div>
				<div class="field">
					<label>Volatility</label> <input type="text" name="volatility"
						placeholder="%">
				</div>
				<div class="field">
					<label>Risk Free Interest Rate</label> <input type="text"
						name="interest" placeholder="%">
				</div>
			</div>
			<div class="inline fields">
				<label for="selection">Select Option Type:</label>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="selection" value="Put"> <label>Put</label>
					</div>
				</div>
				<div class="field">
					<div class="ui radio checkbox">
						<input type="radio" name="selection" value="Call"> <label>Call</label>
					</div>
				</div>
			</div>
			<br />
			<div style="text-align: center">
				<button type="submit" class="ui blue button">Calculate</button>
			</div>
		</div>
	</form>
</body>
</html>