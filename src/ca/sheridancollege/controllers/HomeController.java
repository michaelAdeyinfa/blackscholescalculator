package ca.sheridancollege.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import ca.sheridancollege.beans.BlackScholesFormula;

@Controller
public class HomeController {
	BlackScholesFormula bsf = new BlackScholesFormula();

	@GetMapping(value = "/")
	public String home(Model model) {

		return "home";
	}

	@PostMapping("/calculateQuote")
	public String determine(Model model, HttpServletRequest request) {
		// assuming it's not a leap year
		double strikePrice = Double.parseDouble(request.getParameter("strikePrice"));
		System.out.println("Strike Price: " + strikePrice);
		double stockPrice = Double.parseDouble(request.getParameter("stockPrice"));
		System.out.println("Stock Price: " + stockPrice);
		double time = Double.parseDouble(request.getParameter("time"));
		double volatility = Double.parseDouble(request.getParameter("volatility")) / 100;
		System.out.println("Volatility: " + volatility);
		double interest = Double.parseDouble(request.getParameter("interest"));
		System.out.println("Interest: " + interest);
		double timeToExpirationDate = time / 365;
		System.out.println("timeToExpirationDate: " + timeToExpirationDate);
		String optionType = request.getParameter("selection");

		boolean isItACall = false;
		if (optionType.equals("Call"))
			isItACall = true;
		double optionVal = bsf.calculate(isItACall, stockPrice, strikePrice, interest, timeToExpirationDate,
				volatility);
		System.out.println("A Call? : " + isItACall + "\nOption Value: " + optionVal);

		model.addAttribute("resultName", optionType);
		model.addAttribute("resultValue", optionVal);
		return "presentQuote";
	}

}
