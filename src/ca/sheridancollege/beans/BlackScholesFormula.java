package ca.sheridancollege.beans;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 
 * The Black�Scholes formula, which gives a theoretical estimate of 
 * the price of European-style options.
 * 
 * The model was first articulated by Fischer Black and Myron Scholes in 
 * their 1973 paper, "The Pricing of Options and Corporate Liabilities", 
 * published in the Journal of Political Economy. They derived a stochastic 
 * partial differential equation, now called the Black�Scholes equation, 
 * which governs the price of the option over time. 
 * 
 * Majority of the code belongs to the author listed below, however, the calculate was 
 * modified by myself to calculate to 2 decimal places.
 * @author mblackford - M. Bret Blackford (credit to Dhruba Bandopadhyay)
 * Source Code: https://github.com/bret-blackford/black-scholes/blob/master/OptionValuation/src/mBret/options/BlackScholesFormula.java
 * 
 */
public class BlackScholesFormula {

	// The Abramowitz & Stegun (1964) numerical approximation
	// below uses six constant values in its formula.
	private final double P = 0.2316419;
	private final double B1 = 0.319381530;
	private final double B2 = -0.356563782;
	private final double B3 = 1.781477937;
	private final double B4 = -1.821255978;
	private final double B5 = 1.330274429;

	/**
	 * 
	 * @param callOption
	 *            boolean true/false
	 * @param s
	 *            = Spot price of underlying stock/asset
	 * @param k
	 *            = Strike price
	 * @param r
	 *            = Risk free annual interest rate continuously compounded
	 * @param t
	 *            = Time in years until option expiration (maturity)
	 * @param v
	 *            = Implied volatility of returns of underlying stock/asset
	 * @return
	 */
	public double calculate(boolean callOption, double s, double k,
			double r, double t, double v) {
	
		//System.out.println("    ----- ");
		//System.out.println(" in BlackScholesFormula:calculate(" + callOption + "," + s + "," + k + "," + r + "," + t + "," + v);
		
		double blackScholesOptionPrice = 0.0;
		
		if (callOption) {
			double cd1 = cumulativeDistribution(d1(s, k, r, t, v));
			double cd2 = cumulativeDistribution(d2(s, k, r, t, v));
			
			blackScholesOptionPrice = s * cd1 - k * Math.exp(-r * t) * cd2;
		} else {
			double cd1 = cumulativeDistribution(-d1(s, k, r, t, v));
			double cd2 = cumulativeDistribution(-d2(s, k, r, t, v));
			
			blackScholesOptionPrice = k * Math.exp(-r * t) * cd2 - s * cd1;
		}
		
		// now round price to 2 decimal places using BigDecimal
		BigDecimal bsop = new BigDecimal(blackScholesOptionPrice);
		bsop = bsop.setScale(2, RoundingMode.HALF_UP);
	    return bsop.doubleValue();
	
	}


	/**
	 * 
	 * @param s
	 *            = Spot price of underlying stock/asset
	 * @param k
	 *            = Strike price
	 * @param r
	 *            = Risk free annual interest rate continuously compounded
	 * @param t
	 *            = Time in years until option expiration (maturity)
	 * @param v
	 *            = Implied volatility of returns of underlying stock/asset
	 * @return
	 */
	private double d1(double s, double k, double r, double t, double v) {
		
		double top = Math.log(s / k) + (r + Math.pow(v, 2) / 2) * t;
		double bottom = v * Math.sqrt(t);
		
		return top / bottom;
	}

	/**
	 * 
	 * @param s
	 *            = Spot price of underlying stock/asset
	 * @param k
	 *            = Strike price
	 * @param r
	 *            = Risk free annual interest rate continuously compounded
	 * @param t
	 *            = Time in years until option expiration (maturity)
	 * @param v
	 *            = Implied volatility of returns of underlying stock/asset
	 * @return
	 */
	private double d2(double s, double k, double r, double t, double v) {
		return d1(s, k, r, t, v) - v * Math.sqrt(t);
	}

	public double cumulativeDistribution(double x) {
		
		//System.out.println(" in BlackScholesFormula:cumulativeDitibution(" + x + ")");
		
		double t = 1 / (1 + P * Math.abs(x));
		double t1 = B1 * Math.pow(t, 1);
		double t2 = B2 * Math.pow(t, 2);
		double t3 = B3 * Math.pow(t, 3);
		double t4 = B4 * Math.pow(t, 4);
		double t5 = B5 * Math.pow(t, 5);
		double b = t1 + t2 + t3 + t4 + t5;
		
		double snd = standardNormalDistribution(x); //for testing
		double cd = 1 - (snd * b);
		
		double resp = 0.0;
		if( x < 0 ) {
			resp = 1 - cd;
		} else {
			resp = cd;
		}
		
		//return x < 0 ? 1 - cd : cd;
		return resp;
	}

	/**
	 * The Abramowitz & Stegun numerical approximation above uses six constant
	 * values in its formula. However it also relies on another function in turn
	 * � the standard normal probability density function (PDF)
	 * 
	 * @param x
	 * @return
	 */
	public double standardNormalDistribution(double x) {
		
		//System.out.println(" in BlackScholesFormula:standardNormalDistribution(" + x + ")");
		double top = Math.exp(-0.5 * Math.pow(x, 2));
		double bottom = Math.sqrt(2 * Math.PI);
		double resp = top / bottom;		
		
		return resp;
	}

}
