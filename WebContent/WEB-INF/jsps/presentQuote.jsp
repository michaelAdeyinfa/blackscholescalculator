<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Computed Quote</title>
<link rel="stylesheet" type="text/css" href="css/semantic.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
<script src="js/semantic.min.js"></script>
</head>
<body>
	<div style="text-align: center">
		<h1>Quote Calculated</h1>
	</div>
	<br>
	<br>
	<div style="text-align: center" class="ui success message">
		<div class="header">$${resultValue}</div>
		<p>Price of ${resultName} Option.</p>
	</div>
	<c:url var="rootUrl" value="/" />
	<div style="text-align: center">
		<a href="${rootUrl}"><button class="ui small button">Go
				back.</button></a>
	</div>
</body>
</html>