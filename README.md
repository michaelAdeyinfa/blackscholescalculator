# BlackScholesCalculator

The Black Scholes model is a model of price variation over time of financial instruments such as stocks that can, among other things, be used to determine the price of a European call option or European put option. [Link to Wiki for more information](https://en.wikipedia.org/wiki/Black%E2%80%93Scholes_model).

This project will be creating a Java Spring MVC application to display form input, calculation and the resulting option price.

**NOTE:** Java is not ideal by any stretch of the imagination when attempting to implement the Black Scholes Model for real-life option valuations (slow performance compared to C/C++).

- View: JSPs using Semantic UI + JQuery + HTML5. 
- Controller: Featuring @GetMapping @PostMapping and HttpServletRequest.
- Model: Not really using a 'model', more like a utility class where the bulk of the calculation is made.

*Run application using Eclipse IDE with Maven and Tomcat*.
